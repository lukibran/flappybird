

$().ready(function() {
	var pos = 0;
	var date = Date.now();
	var floorCurrentLeft = 0;
	var pipeY = Math.floor(Math.random()*100);
	var pipe2Y = Math.floor(Math.random()*100);
	var offset = +150;
	var pipespeed = 1.6;
	var pipeX = 0;
	var pipe2X = offset;
	var gravity = 0.25;
	var velocity = 0;
	var posX = 0;
	var rotation = 0;
	var jump = -4.6;
	var posY = 0;
	var gamestat = 0;
	var betweenpipe1 = false;
	var betweenpipe2 = false;
	var score = 0;
	var oldScore = 2;
	var flashtime = 0;
	/*
	gamestat = 0;	Start Screen
	gamestat = 1;	Tod
	gamestat = 2;	Spielen
	*/

	var speed = 0.1;
	var button = $('.replaybutton');
	var score = $('.score');
	var single = $('.singles');
	var overlay = $('.overlay');
	var player = $('#player');

	function updatePlayer(player)
	{
	   	//rotation
	   	rotation = Math.min((velocity / 10) * 90, 90);
	   
	   	//apply rotation and position
	   	$(player).css({ rotate: rotation, top: posX });
	}


		
	//Handle mouse down OR touch start
	if("ontouchstart" in window)
		$(document).on("touchstart", screenClick);
	else
		$(document).on("mousedown", screenClick);

	function screenClick()
	{
		if(gamestat==0)
		{
			gamestat = 2;
			score = 0;
		}
	      
	   		velocity = jump;	


	      if(gamestat==1)
	      {
	      	posX = 0;
		 	rotation = 0;
		 	pipeX = 0;
		 	pipe2X = 150;
		 	posY = 0;
	      	gamestat=2;
	      	betweenpipe1 = false;
	      	betweenpipe2 = false;
	      	flashtime = 0;
	      	overlay.css('visibility', 'hidden');
	      	score = 0;
	      }
	}

	function death()
	{
		gamestat = 1;	 
		flashtime = 0;
		button.css('visibility', 'visible');
	}

		

	var update = function() 
	{
		var now = Date.now();
		var deltaTime = now - date;
		var bird = $('.flappy');

		if(gamestat == 2)
		{

			button.css('visibility', 'hidden');

			//Bird 
			
			velocity += gravity;
			posY += velocity;
			bird.css('-moz-transform', 'translate(' + posX + 'px,' + posY + 'px) rotate(' + rotation + 'deg)');
			updatePlayer(player);


			//groundfloor Funktion
			var floor = $('.floor');
			floorCurrentLeft -= deltaTime * speed;
			if (floorCurrentLeft <= -48) {
				floorCurrentLeft += 48;
			}
			floor.css('background-position', (-584 + floorCurrentLeft) + 'px 0px');

			//pipe1 Funktion
			pipeX -= pipespeed;
			var pipe = $('.pipe1');
			pipe.css('-moz-transform', 'translate(' + pipeX + 'px, ' + pipeY + 'px)');
			if(pipeX <= -350)
			{
				pipeY = Math.floor(Math.random()*100);
				pipeX=0;
			}

			//pipe2
			pipe2X -= pipespeed;
			var pipe2 = $('.pipe2');
			pipe2.css('visibility', 'visible');
			pipe2.css('-moz-transform', 'translate(' + pipe2X + 'px, ' + pipe2Y + 'px)');

			
			if(pipe2X < 0)
			{
				pipe2.css('visibility', 'visible');
			}
			
			if(pipe2X <= -350)
			{
				pipe2Y = Math.floor(Math.random()*100);
				pipe2X = 0;
			}

			//Collision

			//Ground
		    if(bird.offset().top +15 >= $(".floor").offset().top)
		    {
		    	death();
		    }

		    //Pipe 1
		    if(bird.offset().left + 34 > pipe.offset().left && bird.offset().left < pipe.offset().left + 52 && !betweenpipe1)
			{
				betweenpipe1 = true;
			}
			
			if(bird.offset().left > pipe.offset().left + 52 && betweenpipe1)
			{
				betweenpipe1 = false;
				score++;
			}


			var pipehole1 = $(".pipe1 .free-pipe");
			if(betweenpipe1 && bird.offset().top < pipehole1.offset().top)
			{
				death();
			}
			if(betweenpipe1 &&bird.offset().top + 24 > pipehole1.offset().top + 80)
			{
				death();
			}
			
			//Pipe 2 
			if(bird.offset().left + 34 > pipe2.offset().left && bird.offset().left < pipe2.offset().left + 52 && !betweenpipe2)
			{
				betweenpipe2 = true;
			}
			
			if(bird.offset().left > pipe2.offset().left + 52 && betweenpipe2)
			{
				betweenpipe2 = false;
				score++;
			}

			var pipehole2 = $(".pipe2 .free-pipe");
			if(betweenpipe2 &&bird.offset().top < pipehole2.offset().top)
			{
				death();
				
			}
			if(betweenpipe2 &&bird.offset().top + 24 > pipehole2.offset().top + 80)
			{
				death();

			}

			if (score > 999) {
				score = 999;
			}

			if (oldScore != score) 
			{
				var str = score.toString();

				if (str.length == 1) 
				{
					str = '..' + str;
				} 
				else if(str.length == 2)
				{
					str = '.' + str;
				}

				$('#dig0').attr('class', 'digit');
				$('#dig1').attr('class', 'digit');
				$('#dig2').attr('class', 'digit');

				$('#dig0').addClass('pos' + (str[0] != '.' ? str[0] : ''));
				$('#dig1').addClass('pos' + (str[1] != '.' ? str[1] : ''));
				$('#dig2').addClass('pos' + (str[2] != '.' ? str[2] : ''));
			
				oldScore = score;
			}

		}
		else if(gamestat == 0)
		{
			button.css('visibility', 'visible');

			//groundfloor Funktion
			var floor = $('.floor');
			floorCurrentLeft -= deltaTime * speed;
			if (floorCurrentLeft <= -48) {
				floorCurrentLeft += 48;
			}
			floor.css('background-position', (-584 + floorCurrentLeft) + 'px 0px');

		}
		else if(gamestat == 1)
		{
			flashtime += deltaTime;
			if(flashtime<100)
			{
				overlay.css('visibility', 'visible');
			}
			else
			{
				overlay.css('visibility', 'hidden');
			}
			
			velocity += gravity;
			posY += velocity; 
			rotation = 90;
			if(posY < 187)
			{
				bird.css('-moz-transform', 'translate(' + posX + 'px,' + posY + 'px) rotate(' + rotation + 'deg)');
			}
			
			
			
			
		}

		date = now;
		requestAnimationFrame(update);
	};
	requestAnimationFrame(update);
});

